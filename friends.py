import requests
import datetime
import operator


def calc_age(uid):
    TOKEN = None  # insert api token here
    USER_URL = 'https://api.vk.com/method/users.get?v=5.71&access_token={}&user_ids={}'.format(TOKEN, uid)
    
    user_response = requests.get(USER_URL)
    user_id = user_response.json()['response'][0]['id']
    
    friends_url = 'https://api.vk.com/method/friends.get?v=5.71&access_TOKEN={}&user_id={}&fields=bdate'.format(TOKEN, user_id)
    friends_response = requests.get(friends_url)
    
    bdays = [item.get('bdate') for item in friends_response.json()['response']['items']]
    bdays = list(filter(lambda item: item is not None and len(item) > 6, bdays))
    byears = [bday.split('.')[2] for bday in bdays]
    
    current_year = datetime.datetime.now().year
    
    ages = [current_year - int(byear) for byear in byears]
    ages_counts = dict()
    
    for age in ages:
        if age not in ages_counts:
            ages_counts[age] = 1
        else:
            ages_counts[age] += 1
            
    ages_counts_sorted = sorted([(age, count) for age, count in ages_counts.items()])
    
    return sorted(ages_counts_sorted, key=operator.itemgetter(1), reverse=True)
